// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Net/UnrealNetwork.h"
#include "GameFramework/Actor.h"
#include "RandomMazeGenerator.generated.h"

UCLASS()
class ADVGAMESPROGRAMMING_API ARandomMazeGenerator : public AActor
{
	GENERATED_BODY()
	
public:
    
	// Sets default values for this actor's properties
	ARandomMazeGenerator();
    

protected:

    UPROPERTY(EditDefaultsOnly, Category = "Maze Settings")
    TSubclassOf<class AMazeCell> CellToSwapClass = nullptr;
    
    UPROPERTY(EditDefaultsOnly, Category = "Maze Settings")
    TSubclassOf<class ANavigationNode> NavigationNodeClass;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Maze Settings")
    int32 RowNumber = 5;
    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Maze Settings")
    int32 ColumnNumber = 5;


    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Maze Settings")
    float XDistance = 100; //distance between cells

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Maze Settings")
    float YDistance = 100; //distance between cells



protected:

    virtual void BeginPlay() override;

public:

    virtual void Tick(float DeltaTime) override;
    virtual bool ShouldTickIfViewportsOnly() const override;
    
    UPROPERTY(EditAnywhere)
    bool bRegenerateMap;


private:

    TArray<AMazeCell*> CellList;
    float CurrentDeltaTime = 0.0f;
    bool bRandomizeMaze = false;


    int32 VisitedCells;
    int32 CurrentIndexCell;
    TArray<int32> CellStack;
    
    UPROPERTY(EditAnywhere)
    class AAIManager* Manager;


private:

    int32 GetAdjacentCellWithWalls(const int32& IndexCell);

    void CreateMaze();
    void CreateMap();

};
