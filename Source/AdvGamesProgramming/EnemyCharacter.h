// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Perception/AIPerceptionTypes.h"
#include "DrawDebugHelpers.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"
#include "Perception/AISenseConfig.h"
#include "EnemyCharacter.generated.h"

UENUM()
enum class AgentState : uint8
{
	PATROL,
	ENGAGE,
	EVADE,
    INVESTIGATE
};

UCLASS()
class ADVGAMESPROGRAMMING_API AEnemyCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AEnemyCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	

	TArray <class ANavigationNode* > Path;
	ANavigationNode* CurrentNode;
	class AAIManager* Manager;

	class UHealthComponent* HealthComponent;

	UPROPERTY(VisibleAnywhere, Category = "AI")
	AgentState CurrentAgentState;

	class UAIPerceptionComponent* PerceptionComponent;
	AActor* DetectedActor;
	bool bCanSeeActor;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

    
    virtual void MoveBlockedBy(const FHitResult& Impact) override;
    UFUNCTION(Server, Reliable)
    void ServerMoveBlockedBy();

    UFUNCTION(Server, Reliable)
	void ServerAgentPatrol();
    UFUNCTION(Server, Reliable)
	void ServerAgentEngage();
    UFUNCTION(Server, Reliable)
	void ServerAgentEvade();
    UFUNCTION(Server, Reliable)
    void ServerAgentInvestigate();

	UFUNCTION()
	void SensePlayer(AActor* ActorSensed, FAIStimulus Stimulus);

	UFUNCTION(BlueprintImplementableEvent)
	void Fire(FVector FireDirection);
    
    UFUNCTION(BlueprintImplementableEvent)
    void ToggleCrouch(bool Enable);

private:
    bool bIsCrouching;
    bool bIsInvestigating;
    bool bCanHearActor;

    UFUNCTION(Server, Reliable)
	void ServerMoveAlongPath();
    UFUNCTION(Server, Reliable)
    void ServerFacePlayer(float DeltaTime);
    bool HitFeet();
    bool HitHead();


};
