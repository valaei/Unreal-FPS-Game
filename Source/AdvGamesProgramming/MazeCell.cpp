// Fill out your copyright notice in the Description page of Project Settings.


#include "MazeCell.h"
#include "Components/StaticMeshComponent.h"
#include "Components/TextRenderComponent.h"

AMazeCell::AMazeCell()
{
    //adding static mesh components
    RootScene = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
    RootComponent = RootScene;

    Wall_0 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Wall_0"));
    Wall_0->SetupAttachment(RootComponent);

    Wall_1 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Wall_1"));
    Wall_1->SetupAttachment(RootComponent);

    Wall_2 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Wall_2"));
    Wall_2->SetupAttachment(RootComponent);

    Wall_3 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Wall_3"));
    Wall_3->SetupAttachment(RootComponent);


    PrimaryActorTick.bCanEverTick = false;
    bWall0Path = false;
    bWall1Path = false;
    bWall2Path = false;
    bWall3Path = false;
    
}

// Called when the game starts or when spawned
void AMazeCell::BeginPlay()
{
    Super::BeginPlay();

    NumberOfActiveWalls = 4; //counter for if the walls are all up
    bCreated = false;


}
//initializing
void AMazeCell::Initialize(int32 Row, int32 Column, int32 Index)
{
    bCreated = false;

    RowIndex = Row;
    ColumnIndex = Column;

    NumberOfActiveWalls = 4;


}

void AMazeCell::ShowWall(int32 WallIndex) //showing walls by setting visibility to true
{
    switch (WallIndex)
    {
        case 0:
            Wall_0->SetVisibility(true, true); // Front Wall
            break;
        case 1:
            Wall_1->SetVisibility(true, true); // Back wall
            break;
        case 2:
            Wall_2->SetVisibility(true, true); // Left wall
            break;
        case 3:
            Wall_3->SetVisibility(true, true); // Right wall
            break;
    }
}

void AMazeCell::HideWall(int32 WallIndex) //hiding walls by setting visibility to false
{
    switch (WallIndex)
    {
        case 0:
            Wall_0->SetVisibility(false, true); // Front Wall
            Wall_0->SetCollisionProfileName(TEXT("NoCollision"));
            NumberOfActiveWalls -= 1;
            break;
        case 1:
            Wall_1->SetVisibility(false, true); // Back wall
            Wall_1->SetCollisionProfileName(TEXT("NoCollision"));
            NumberOfActiveWalls -= 1;
            break;
        case 2:
            Wall_2->SetVisibility(false, true); // Left wall
            Wall_2->SetCollisionProfileName(TEXT("NoCollision"));
            NumberOfActiveWalls -= 1;
            break;
        case 3:
            Wall_3->SetVisibility(false, true); // Right wall
            Wall_3->SetCollisionProfileName(TEXT("NoCollision"));
            NumberOfActiveWalls -= 1;
            break;
    }
}


void AMazeCell::CreatePassage(int32 WallIndex) //similar to hiding a wall, this is to create a passage
{
    switch (WallIndex)
    {
        case 0:
            if(!bWall0Path)
            {
                GenerateRandomPassage(Wall_0);
                NumberOfActiveWalls -= 1;
                bWall0Path = true;
            }
            break;
        case 1:
            if(!bWall1Path)
            {
                GenerateRandomPassage(Wall_1);
                NumberOfActiveWalls -= 1;
                bWall1Path = true;
            }
            break;
        case 2:
            if(!bWall2Path)
            {
                GenerateRandomPassage(Wall_2);
                NumberOfActiveWalls -= 1;
                bWall2Path = true;
            }
            break;
        case 3:
            if(!bWall3Path)
            {
                GenerateRandomPassage(Wall_3);
                NumberOfActiveWalls -= 1;
                bWall3Path = true;
            }
            break;
    }
}

void AMazeCell::GenerateRandomPassage(UStaticMeshComponent* Wall)
{
    int RandomPassage = FMath::RandRange(0, 3);
    if (RandomPassage < 2)
    {
        Wall->SetVisibility(false, true);
        Wall->SetCollisionProfileName(TEXT("NoCollision"));
    }
    else if (RandomPassage == 2)
    {
        Wall->AddWorldOffset(FVector(0.0f, 0.0f, -350.0f));


    }
    else if (RandomPassage == 3)
    {
        Wall->SetWorldScale3D(FVector(1.0f, 1.0f, 0.3f));
        Wall->AddWorldOffset(FVector(0.0f, 0.0f, 150.0f));
    }
}



