// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyCharacter.h"
#include "AIManager.h"
#include "NavigationNode.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Perception/AIPerceptionComponent.h"
#include "HealthComponent.h"

// Sets default values
AEnemyCharacter::AEnemyCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CurrentAgentState = AgentState::PATROL;
}

// Called when the game starts or when spawned
void AEnemyCharacter::BeginPlay()
{
	Super::BeginPlay();
	
	Cast<UCharacterMovementComponent>(GetMovementComponent())->bOrientRotationToMovement = true;

	HealthComponent = FindComponentByClass<UHealthComponent>();

	PerceptionComponent = FindComponentByClass<UAIPerceptionComponent>();
	if (PerceptionComponent)
	{
		PerceptionComponent->OnTargetPerceptionUpdated.AddDynamic(this, &AEnemyCharacter::SensePlayer);
	}

	DetectedActor = nullptr;
	bCanSeeActor = false;
    bCanHearActor = false;
    bIsCrouching = false;
    bIsInvestigating = false;
}

// Called every frame
void AEnemyCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

    if (CurrentAgentState == AgentState::PATROL)
	{
		ServerAgentPatrol();
		if (bCanSeeActor && HealthComponent->HealthPercentageRemaining() >= 40.0f)
		{
			CurrentAgentState = AgentState::ENGAGE;
			Path.Empty();
		} 
		else if (bCanSeeActor && HealthComponent->HealthPercentageRemaining() < 40.0f)
		{
			CurrentAgentState = AgentState::EVADE;
			Path.Empty();
		}
        else if (bCanHearActor)
        {
            CurrentAgentState = AgentState::INVESTIGATE;
            bIsInvestigating = true;
            bCanHearActor = false;
            Path.Empty();
        }
	}
	else if (CurrentAgentState == AgentState::ENGAGE)
	{
		ServerAgentEngage();
		if (!bCanSeeActor)
		{
			CurrentAgentState = AgentState::INVESTIGATE;
            bIsInvestigating = true;
		}
		else if (bCanSeeActor && HealthComponent->HealthPercentageRemaining() < 40.0f)
		{
			CurrentAgentState = AgentState::EVADE;
			Path.Empty();
		}
	}
	else if (CurrentAgentState == AgentState::EVADE)
	{
		ServerAgentEvade();
		if (!bCanSeeActor)
		{
			CurrentAgentState = AgentState::PATROL;
		}
		else if (bCanSeeActor && HealthComponent->HealthPercentageRemaining() >= 40.0f)
		{
			CurrentAgentState = AgentState::ENGAGE;
			Path.Empty();
		}
	}
    else if (CurrentAgentState == AgentState::INVESTIGATE)
    {
        ServerAgentInvestigate();
        if (!bCanSeeActor && !bIsInvestigating)
        {
            CurrentAgentState = AgentState::PATROL;
        }
        else if (bCanSeeActor && HealthComponent->HealthPercentageRemaining() >= 40.0f)
        {
            bIsInvestigating = false;
            CurrentAgentState = AgentState::ENGAGE;
            Path.Empty();
        }
        else if (bCanSeeActor && HealthComponent->HealthPercentageRemaining() < 40.0f)
        {
            bIsInvestigating = false;
            CurrentAgentState = AgentState::EVADE;
            Path.Empty();
        }
    }

    ServerMoveAlongPath();
    ServerFacePlayer(DeltaTime);


}

// Called to bind functionality to input
void AEnemyCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AEnemyCharacter::ServerAgentPatrol_Implementation()
{
	if (Path.Num() == 0)
	{
		if (Manager)
		{
			Path = Manager->GeneratePath(CurrentNode, Manager->AllNodes[FMath::RandRange(0, Manager->AllNodes.Num() - 1)]);
		}
	}
}

void AEnemyCharacter::ServerAgentEngage_Implementation()
{
	if (bCanSeeActor && DetectedActor)
	{
		FVector FireDirection = DetectedActor->GetActorLocation() - GetActorLocation();
		Fire(FireDirection);
	}
	if (Path.Num() == 0 && DetectedActor)
	{
		ANavigationNode* NearestNode = Manager->FindNearestNode(DetectedActor->GetActorLocation());
		Path = Manager->GeneratePath(CurrentNode, NearestNode);
	}
}

void AEnemyCharacter::ServerAgentEvade_Implementation()
{
	if (bCanSeeActor && DetectedActor)
	{
		FVector FireDirection = DetectedActor->GetActorLocation() - GetActorLocation();
		Fire(FireDirection);
	}
	if (Path.Num() == 0 && DetectedActor)
	{
		ANavigationNode* FurthestNode = Manager->FindFurthestNode(DetectedActor->GetActorLocation());
		Path = Manager->GeneratePath(CurrentNode, FurthestNode);
	}
}

void AEnemyCharacter::ServerAgentInvestigate_Implementation()
{
    if (bCanSeeActor && DetectedActor)
    {
        FVector FireDirection = DetectedActor->GetActorLocation() - GetActorLocation();
        Fire(FireDirection);
    }
    if (Path.Num() == 0 && DetectedActor && bIsInvestigating)
    {
        ANavigationNode* NearestNode = Manager->FindNearestNode(DetectedActor->GetActorLocation());
        Path = Manager->GeneratePath(CurrentNode, NearestNode);

        // Connect Adjacent Nodes and Create Investigate Path
        ANavigationNode* LastNode = NearestNode;
        TArray<ANavigationNode*> ConnectedNodes = NearestNode->ConnectedNodes;
        for (ANavigationNode* Node : ConnectedNodes)
        {
            Path.Insert(Manager->GeneratePath(LastNode, Node), 0);
            LastNode = Node;
        }
        bIsInvestigating = false;
    }
}

void AEnemyCharacter::SensePlayer(AActor* ActorSensed, FAIStimulus Stimulus)
{
	if (Stimulus.WasSuccessfullySensed())
	{
        // Check Whether It's triggered By Sight or Hearing
        FString SenseName = PerceptionComponent->GetSenseConfig(Stimulus.Type)->GetSenseName();
        //UE_LOG(LogTemp, Warning, TEXT("Player Detected By %s"), *SenseName);
		DetectedActor = ActorSensed;
		if (SenseName.Equals(TEXT("Sight")))
        {
            bCanSeeActor = true;
        }
        else if (SenseName.Equals(TEXT("Hearing")))
        {
            bCanHearActor = true;
        }
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Player Lost"))
		bCanSeeActor = false;
        bCanHearActor = false;
	}
}

void AEnemyCharacter::ServerMoveAlongPath_Implementation()
{
    
    if ((GetActorLocation() - CurrentNode->GetActorLocation()).IsNearlyZero(100.0f)
        && Path.Num() > 0)
    {
        CurrentNode = Path.Pop();
        
        // If It's Crouching, Uncrouch
        if (bIsCrouching)
        {
            ToggleCrouch(false);
            bIsCrouching = false;
        }
    }
    else if (!(GetActorLocation() - CurrentNode->GetActorLocation()).IsNearlyZero(100.0f))
    {
        AddMovementInput(CurrentNode->GetActorLocation() - GetActorLocation());
    }
    
}

bool AEnemyCharacter::HitHead()
{
    FVector MoveDirection = CurrentNode->GetActorLocation() - GetActorLocation();
    MoveDirection.Z = 0;
    MoveDirection.GetSafeNormal(0.0001f);
    MoveDirection.Normalize(0.0001f);
    FHitResult HitResult;
    FVector Start = GetActorLocation() + FVector(0.0f, 0.0f, 50.0f) - 50.0 * MoveDirection;
    FVector End = Start + 100.0 * MoveDirection;
    FCollisionQueryParams Params;
    Params.AddIgnoredActor(this);

    bool bIsHit = GetWorld()->LineTraceSingleByChannel(HitResult, Start, End, ECollisionChannel::ECC_GameTraceChannel1, Params);
    
    DrawDebugLine(GetWorld(), Start, End, FColor(0, 255, 0), false, -1, 0, 5.0);
    
    if (bIsHit)
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool AEnemyCharacter::HitFeet()
{
    FVector MoveDirection = CurrentNode->GetActorLocation() - GetActorLocation();
    MoveDirection.Z = 0;
    MoveDirection.GetSafeNormal(0.0001f);
    MoveDirection.Normalize(0.0001f);
    FHitResult HitResult;
    FVector Start = GetActorLocation() - 50.0 * MoveDirection;
    Start.Z = 10.0f;
    FVector End = Start + 100.0 * MoveDirection;
    FCollisionQueryParams Params;
    Params.AddIgnoredActor(this);

    bool bIsHit = GetWorld()->LineTraceSingleByChannel(HitResult, Start, End, ECollisionChannel::ECC_GameTraceChannel1, Params);
    
    DrawDebugLine(GetWorld(), Start, End, FColor(255, 0, 0), false, -1, 0, 5.0);
    
    if (bIsHit)
    {
        return true;
    }
    else
    {
        return false;
    }
}

void AEnemyCharacter::MoveBlockedBy(const FHitResult& Impact)
{
    ServerMoveBlockedBy();
}

void AEnemyCharacter::ServerMoveBlockedBy_Implementation()
{
    //if (Impact.GetActor() == NULL)
    //{
        if (HitFeet() && !HitHead())
        {
            Jump();
            UE_LOG(LogTemp, Warning, TEXT("Jump"));
        }
        else if (!HitFeet() && HitHead())
        {
            ToggleCrouch(true);
            bIsCrouching = true;
        }
    //}
}

void AEnemyCharacter::ServerFacePlayer_Implementation(float DeltaTime)
{
    if (bCanSeeActor && DetectedActor)
    {
        FRotator LookAt = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), DetectedActor->GetActorLocation());
        FRotator NewRot = FMath::RInterpTo(GetActorRotation(), LookAt, DeltaTime, 20.0f);
        SetActorRotation(FRotator(0.0f, NewRot.Yaw, 0.0f));
    }
}

