// Fill out your copyright notice in the Description page of Project Settings.

#include "Engine/World.h"
#include "RandomMazeGenerator.h"
#include "AIManager.h"
#include "NavigationNode.h"
#include "MazeCell.h"



ARandomMazeGenerator::ARandomMazeGenerator()
{
    PrimaryActorTick.bCanEverTick = true;
    bRegenerateMap = false;

}


void ARandomMazeGenerator::BeginPlay()
{
    Super::BeginPlay();
}


void ARandomMazeGenerator::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
    if (bRegenerateMap)
    {
        CellList.Empty();
        CellStack.Empty();
        VisitedCells = 0;
        TArray<AActor*> AttachedActors;
        GetAttachedActors(AttachedActors);
        for (auto* Attached : AttachedActors)
        {
           Attached->Destroy();
        }
        CreateMap();
        CreateMaze();
        bRegenerateMap = false;
    }

}

bool ARandomMazeGenerator::ShouldTickIfViewportsOnly() const
{
    return true;
}

void ARandomMazeGenerator::CreateMap()
{
    FVector SpawnPosition = GetActorLocation();
    //we spawn all the cell grids first with this for loop
    for (int iRow = 0; iRow < RowNumber; ++iRow)
    {
        SpawnPosition.X = GetActorLocation().X;

        for (int iCol = 0; iCol < ColumnNumber; ++iCol)
        {
            // Spawn each wall
            FActorSpawnParameters SpawnParameters;
            SpawnParameters.Owner = this;

            AMazeCell* SpawnedCell = GetWorld()->SpawnActor<AMazeCell>(CellToSwapClass, SpawnPosition, FRotator::ZeroRotator, SpawnParameters);
            
            ANavigationNode* SpawnedNode = GetWorld()->SpawnActor<ANavigationNode>(NavigationNodeClass, SpawnPosition + FVector(200.0f, 200.0f, 0.0f), FRotator::ZeroRotator);
            
            SpawnedNode->AttachToActor(this, FAttachmentTransformRules::KeepWorldTransform);

            
            SpawnedCell->NavigationNode = SpawnedNode;


            if (SpawnedCell != nullptr)
            {
                // this is to keep everything in the same actor so spawnedcell will be attached to the generator actor
                SpawnedCell->AttachToActor(this, FAttachmentTransformRules::KeepWorldTransform);

                int32 CellIndex = iRow * ColumnNumber + iCol; //formula to get the index in a list

                SpawnedCell->Initialize(iRow, iCol, CellIndex);

                CellList.Add(SpawnedCell);
            }

            SpawnPosition.X += XDistance;
        }

        SpawnPosition.Y += YDistance;
    }

    // generate a random row and column and get the current index
    int32 RandomRow = FMath::RandRange(0, RowNumber - 1);
    int32 RandomCol = FMath::RandRange(0, ColumnNumber - 1);

    CurrentIndexCell = RandomRow * ColumnNumber + RandomCol; //formula to get the index

    // show set cube of walls
    CellList[CurrentIndexCell]->SetIsCreated(true);
    CellList[CurrentIndexCell]->ShowWall(0);
    CellList[CurrentIndexCell]->ShowWall(1);
    CellList[CurrentIndexCell]->ShowWall(2);
    CellList[CurrentIndexCell]->ShowWall(3);

    // adding it to a stack to pop and push
    CellStack.Push(CurrentIndexCell);
    VisitedCells = 1;
    // randomize maze
    bRandomizeMaze = true;
}

void ARandomMazeGenerator::CreateMaze()
{
    if (bRandomizeMaze)
    {

        while (VisitedCells < CellList.Num())
        {
            // Find neighbour and create a passage for that wall
            int32 NextIndexCell = GetAdjacentCellWithWalls(CurrentIndexCell);
            
            if (NextIndexCell > -1)
            {
                // Push the current cell
                CellStack.Push(CurrentIndexCell);
                CurrentIndexCell = NextIndexCell;
                VisitedCells++;
            }
            else
            {
                // Get from the stack
                if (CellStack.Num() > 0)
                {
                    CurrentIndexCell = CellStack.Pop();
                }
                else
                {
                    VisitedCells = CellList.Num();

                    UE_LOG(LogTemp, Warning, TEXT("generation completed"));

                    bRandomizeMaze = false;
                }
            }
        }
        VisitedCells = CellList.Num();
        bRandomizeMaze = false;
    }

    //to create an entrance and exit to the maze
    CellList[0]->HideWall(2);

    CellList[RowNumber * ColumnNumber - 1]->HideWall(3);
}


//this is to get the neighboring cells
int32 ARandomMazeGenerator::GetAdjacentCellWithWalls(const int32& IndexCell)
{
    TArray<int32> NeighbourCells;
    TArray<int32> Directions; // To store each direction


    // Coordinates of cells
    int32 Col = CellList[IndexCell]->GetCoords().X;
    int32 Row = CellList[IndexCell]->GetCoords().Y;
    //here we check if these cells have all 4 walls connected first then add them
    //front
    if ((Row - 1) >= 0)
    {
        int32 Next = (Row - 1) * ColumnNumber + Col;

        if (CellList[Next]->AllWallsConnected()) // All walls have been generated
        {
            Directions.Add(0);
            NeighbourCells.Add(Next);
        }
    }

    // Bottom
    if ((Row + 1) < RowNumber)
    {
        int32 Next = (Row + 1) * ColumnNumber + Col;

        if (CellList[Next]->AllWallsConnected()) // All walls up for this cell
        {
            Directions.Add(1);
            NeighbourCells.Add(Next);
        }
    }
    // Right
    if ((Col + 1) < ColumnNumber)
    {
        int32 Next = Row * ColumnNumber + (Col + 1);

        if (CellList[Next]->AllWallsConnected()) // All walls up for this cell
        {
            Directions.Add(3);

            NeighbourCells.Add(Next);
        }
    }
    //Left
    if ((Col - 1) >= 0)
    {
        int32 Next = Row * ColumnNumber + (Col - 1);

        if (CellList[Next]->AllWallsConnected()) // All walls up for this cell
        {
            Directions.Add(2);
            NeighbourCells.Add(Next);
        }
    }



    int32 NextCellIndex = -1;

    // Select random direction from available ones
    if (NeighbourCells.Num() > 0)
    {
        int32 RandomDirection = FMath::RandRange(0, Directions.Num() - 1);

        // Knocked down direction between the current cell and next cell
        NextCellIndex = NeighbourCells[RandomDirection];


        // Check if that cell hasn't been created
        if (!CellList[NextCellIndex]->GetIsCreated())
        {
            CellList[NextCellIndex]->SetIsCreated(true);

            CellList[NextCellIndex]->ShowWall(0);
            CellList[NextCellIndex]->ShowWall(1);
            CellList[NextCellIndex]->ShowWall(2);
            CellList[NextCellIndex]->ShowWall(3);

        }
        // Pick the cell as a current cell
        //this is to create a passage by with opposite walls
        CellList[IndexCell]->NavigationNode->ConnectedNodes.Add(CellList[NextCellIndex]->NavigationNode);
        CellList[NextCellIndex]->NavigationNode->ConnectedNodes.Add(CellList[IndexCell]->NavigationNode);

        
        switch (Directions[RandomDirection])
        {
            case 0: // top
                CellList[NextCellIndex]->HideWall(1);
                CellList[IndexCell]->CreatePassage(0);
                break;

            case 1: //down
                CellList[NextCellIndex]->HideWall(0);
                CellList[IndexCell]->CreatePassage(1);
                break;

            case 2: // left
                CellList[NextCellIndex]->HideWall(3);
                CellList[IndexCell]->CreatePassage(2);
                break;

            case 3: // right
                CellList[NextCellIndex]->HideWall(2);
                CellList[IndexCell]->CreatePassage(3);
                break;
        }

    }
    return NextCellIndex;
}
