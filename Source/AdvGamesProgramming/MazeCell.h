// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Math/UnrealMathUtility.h"
#include "Net/UnrealNetwork.h"
#include "NavigationNode.h"
#include "GameFramework/Actor.h"
#include "MazeCell.generated.h"

UCLASS()
class ADVGAMESPROGRAMMING_API AMazeCell : public AActor
{
	GENERATED_BODY()
protected:
    //here we declare all the walls and floors for the blueprint
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "MazeWall")
    class USceneComponent* RootScene; //root

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "MazeWall")
	class UStaticMeshComponent* Wall_0; //Front

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "MazeWall")
	class UStaticMeshComponent* Wall_1; //Right

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "MazeWall")
	class UStaticMeshComponent* Wall_2; //Left

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "MazeWall")
	class UStaticMeshComponent* Wall_3; //Back
    
    



public:

    AMazeCell();

protected:

    virtual void BeginPlay() override;

public:


    void Initialize(int32 Row, int32 Column, int32 Index);

    FORCEINLINE bool GetIsCreated() { return bCreated; };
    FORCEINLINE void SetIsCreated(bool Value) { bCreated = Value; };

    FORCEINLINE bool AllWallsConnected() { return (NumberOfActiveWalls >= 4); };

    //public functions to generate the walls and hide them

    void ShowWall(int32 WallIndex);

    void HideWall(int32 WallIndex);

    void CreatePassage(int32 WallIndex); //for creating a passage between 2 walls
    void GenerateRandomPassage(UStaticMeshComponent* Wall); 

    UFUNCTION(BlueprintImplementableEvent)
    void CreateNode();

    FORCEINLINE FVector2D GetCoords()
    {
        return FVector2D(ColumnIndex, RowIndex);
    }

    UPROPERTY(EditAnywhere)
    class ANavigationNode* NavigationNode;
    
private:
    bool bCreated;
    bool bWall0Path, bWall1Path, bWall2Path, bWall3Path;
    int32 NumberOfActiveWalls = 0;
    int32 ColumnIndex;
    int32 RowIndex;
    

};
