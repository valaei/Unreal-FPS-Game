# UE4 FPS Game

This is first person shooter game which is designed for UTS Advanced Game Development subject as my final project, using Unreal Engine 4. It features NPC AI, random map generator, networking and multi-player.

![NPC](./images/image-2.png "NPC")
![Map](./images/image-1.png "Map Generator")

